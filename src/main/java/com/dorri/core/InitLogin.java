package com.dorri.core;

import com.akdeniz.googleplaycrawler.GooglePlayAPI;
import com.dorri.racoon.Layout;
import com.dorri.racoon.PlayManager;
import com.dorri.racoon.PlayProfile;
import com.dorri.racoon.net.DroidConnectionSocketFactory;
import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class InitLogin {

    @Inject
    Global global;
    @ConfigProperty(name = "app.login.username")
    String username;

    @ConfigProperty(name = "app.login.password")
    String password;

    public GooglePlayAPI init(String usernameParam, String passwordParam) throws Exception {
        //todo 1- create Layout for handling reports
        PlayProfile pp = new PlayProfile();

        //todo 2- get username & password from property or API or database
        String username;
        String alias;
        String password;
        if (!Utils.isBlank(usernameParam) && !Utils.isBlank(passwordParam)){
            username = usernameParam.trim();
            alias = usernameParam.trim().split("@")[0];
            password = passwordParam;
        }else {
            username = this.username.trim();
            alias = this.username.trim().split("@")[0];
            password = this.password;
        }
        pp.setUser(username.trim());
        pp.setAlias(alias);
        pp.setPassword(password);

        //todo 3- implement createProxyClient in playManager and set proxy if exist
        GooglePlayAPI api = PlayManager.createConnection(pp);
        api.setClient(createLoginClient());
        api.login();
        pp.setToken(api.getToken()); //save this token in database
        //hard-coded
        pp.setAgent("Android-Finsky/30.2.18-21 (api=3,versionCode=83021810,sdk=31,device=r9q,hardware=qcom,product=r9qxeea,platformVersionRelease=12,model=SM-G990B,buildId=SP1A.210812.016)");
        api.setUseragent(pp.getAgent());
        api.checkin(); // Generates the GSF ID
        pp.setGsfId(api.getAndroidID());

        global.add(pp.getClass(), pp);
        global.add(api.getClass(), api);
        global.add(Layout.class, Layout.DEFAULT);

        Log.infof("login with token = {0}", api.getToken());

        return api;
    }

    public static HttpClient createLoginClient() {
        RegistryBuilder<ConnectionSocketFactory> rb = RegistryBuilder.create();
        rb.register("https", new DroidConnectionSocketFactory());
        // rb.register("http", new DroidConnectionSocketFactory());
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(
                rb.build());
        connManager.setMaxTotal(100);
        connManager.setDefaultMaxPerRoute(30);
        // TODO: Increase the max connection limits. If we are doing bulkdownloads,
        // we will download from multiple hosts.
        int timeout = 9;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
        HttpClientBuilder hcb = HttpClientBuilder.create().setDefaultRequestConfig(
                config);

        return hcb.setConnectionManager(connManager).build();
    }

}
