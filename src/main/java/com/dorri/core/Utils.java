package com.dorri.core;

public class Utils {
    public static boolean isBlank(String str) {
        return str == null || str.trim().isEmpty();
    }
}
