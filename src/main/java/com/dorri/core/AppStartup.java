package com.dorri.core;

import com.akdeniz.googleplaycrawler.GooglePlayAPI;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class AppStartup {

    private static final Logger LOGGER = LoggerFactory.getLogger("ListenerBean");

    /**
     * Inject a bean used in the callbacks.
     */
    @Inject
    InitLogin initLogin;

    void onStart(@Observes StartupEvent ev) throws Exception {
        GooglePlayAPI api = initLogin.init(null,null);
        LOGGER.info("The application is starting... ");
        LOGGER.info("Token = {}", api.getToken());
    }

    void onStop(@Observes ShutdownEvent ev) {
        LOGGER.info("The application is stopping... ");
    }
}
