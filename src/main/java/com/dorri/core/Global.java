package com.dorri.core;

import jakarta.inject.Singleton;

import java.util.HashMap;
import java.util.Map;

@Singleton
public class Global {
    public final Map<String, Object> globals= new HashMap<>();

    public <T> void add(Class<? extends T> clazz , T object) {
        globals.put(clazz.getName(), object);
    }

    public <T> void remove(Class<T> clazz) {
        globals.remove(clazz.getName());
    }

    public <T> T get(Class<T> clazz) {
        return clazz.cast(globals.get(clazz.getName()));
    }

}
