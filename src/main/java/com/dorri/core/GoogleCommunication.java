package com.dorri.core;

import com.akdeniz.googleplaycrawler.GooglePlayAPI;
import com.dorri.racoon.gplay.SearchEngineResultPage;
import jakarta.enterprise.context.ApplicationScoped;


@ApplicationScoped
public class GoogleCommunication {

    public SearchEngineResultPage search(GooglePlayAPI api, String query) throws Exception {
        String tmp;
        SearchEngineResultPage serp = new SearchEngineResultPage(SearchEngineResultPage.SEARCH);
        if (query != null) {
            serp.append(api.searchApp(query));
            tmp = serp.getNextPageUrl();
            if (tmp != null && !"".equals(tmp)) {
                serp.append(api.getList(tmp));
            }
        }
        return serp;
    }


}
