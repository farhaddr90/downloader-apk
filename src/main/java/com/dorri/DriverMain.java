package com.dorri;

import com.akdeniz.googleplaycrawler.GooglePlay;
import com.akdeniz.googleplaycrawler.GooglePlayAPI;
import com.dorri.racoon.gplay.SearchEngineResultPage;
import com.dorri.racoon.net.DroidConnectionSocketFactory;
import com.dorri.racoon.PlayManager;
import com.dorri.racoon.PlayProfile;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.io.*;
import java.net.URL;
import java.util.List;

public class DriverMain {

    public static void main1(String[] args) throws Exception {
        //todo 1- create Layout for handling reports
        PlayProfile pp = new PlayProfile();

        //todo 2- get username & password from property or API or database
        String username  = "farhaddr65@gmail.com";
        String password = "rageoffery";
        pp.setUser(username.trim());
        pp.setAlias(username.trim().split("@")[0]);
        pp.setPassword(password);

        //todo 3- implement createProxyClient in playManager and set proxy if exist
        GooglePlayAPI api = PlayManager.createConnection(pp);
        api.setClient(createLoginClient());
        api.login();
        pp.setToken(api.getToken()); //save this token in database
        //hard-coded
        pp.setAgent("Android-Finsky/30.2.18-21 (api=3,versionCode=83021810,sdk=31,device=r9q,hardware=qcom,product=r9qxeea,platformVersionRelease=12,model=SM-G990B,buildId=SP1A.210812.016)");
        api.setUseragent(pp.getAgent());
        api.checkin(); // Generates the GSF ID
        //I think this line is redundant
//        api.login();
//        api.uploadDeviceConfig();
        pp.setGsfId(api.getAndroidID());
        //check if GsfId is changed or not

        //----------search app------------
        SearchEngineResultPage result = search(api, "instagram");
        List<GooglePlay.DocV2> content = result.getContent();

    }

    public static HttpClient createLoginClient() {
        RegistryBuilder<ConnectionSocketFactory> rb = RegistryBuilder.create();
        rb.register("https", new DroidConnectionSocketFactory());
        // rb.register("http", new DroidConnectionSocketFactory());
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(
                rb.build());
        connManager.setMaxTotal(100);
        connManager.setDefaultMaxPerRoute(30);
        // TODO: Increase the max connection limits. If we are doing bulkdownloads,
        // we will download from multiple hosts.
        int timeout = 9;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
        HttpClientBuilder hcb = HttpClientBuilder.create().setDefaultRequestConfig(
                config);

        return hcb.setConnectionManager(connManager).build();
    }
    //-------------------------------search---------------------------------------

    static SearchEngineResultPage search(GooglePlayAPI api, String query) throws Exception {
        String tmp;
        SearchEngineResultPage serp = new SearchEngineResultPage(SearchEngineResultPage.SEARCH);
        if (query != null) {
            serp.append(api.searchApp(query));
            tmp = serp.getNextPageUrl();
            serp.append(api.getList(tmp));
        }
        return serp;
    }

    public static void main(String[] args) throws IOException {
        URL url = new URL("https://ups.music-fa.com/tagdl/1402/Aban%20Baloochi%20-%20Kafe%20Koochehaye%20Shahr%20%28320%29.mp3");
        File fileToWrite = new File("D:/Users2/ggr/Downloads/farhad5.mp3");

        try(FileOutputStream out = new FileOutputStream(fileToWrite);
            BufferedInputStream in = new BufferedInputStream(url.openStream())
            ){
            int readBytes;
            byte[] bytes = new byte[1024];
            while ((readBytes = in.read(bytes, 0 , 1024))!=-1){
                out.write(bytes, 0 , readBytes);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}