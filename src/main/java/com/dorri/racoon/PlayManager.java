package com.dorri.racoon;

import com.akdeniz.googleplaycrawler.GooglePlayAPI;

import java.util.Locale;

public class PlayManager {

    public static GooglePlayAPI createConnection(PlayProfile profile) {
        GooglePlayAPI ret = new GooglePlayAPI(profile.getUser(),
                profile.getPassword());
        ret.setUseragent(profile.getAgent());
        ret.setAndroidID(profile.getGsfId());
        ret.setToken(profile.getToken());
        Locale l = Locale.getDefault();
        String s = l.getLanguage();
        if (l.getCountry() != null) {
            s = s + "-" + l.getCountry();
        }
        ret.setLocalization(s);
//        try {
//            HttpClient proxy = createProxyClient(profile);
//            if (proxy != null) {
//                ret.setClient(proxy);
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
        return ret;
    }

}
