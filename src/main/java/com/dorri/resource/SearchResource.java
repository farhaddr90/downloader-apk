package com.dorri.resource;

import com.akdeniz.googleplaycrawler.GooglePlayAPI;
import com.dorri.racoon.gplay.SearchEngineResultPage;
import com.dorri.core.Global;
import com.dorri.core.GoogleCommunication;
import com.dorri.resource.dto.SearchResultDto;
import jakarta.inject.Inject;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/v1/api")
public class SearchResource {

    @Inject
    GoogleCommunication googleCommunication;

    @Inject
    Global global;

    @Path("/search")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDocs(String query) throws Exception {
        SearchEngineResultPage searchResult = googleCommunication.search(global.get(GooglePlayAPI.class), query);
//        List<GooglePlay.DocV2> docs = searchResult.getContent();
        SearchResultDto searchResultDto = new SearchResultDto(searchResult.getContent().get(0));
        return Response.ok().entity(searchResultDto).build();
    }

}
