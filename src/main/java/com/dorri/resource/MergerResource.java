package com.dorri.resource;

import com.dorri.racoon.Layout;
import com.reandroid.apkeditor.merge.Merger;
import com.reandroid.commons.command.ARGException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;

import java.io.IOException;
import java.nio.file.Paths;

@Path("/v1/merge")
public class MergerResource {

    @Path("/do")
    @POST
    public String merge(String packageName){
        java.nio.file.Path path = Paths.get(Layout.DEFAULT.appsDir.getPath(), packageName);
        String[] args = new String[]{"-i",path.toString()};
        try {
            Merger.execute(args);
            return "successfully done!";
        } catch (ARGException | IOException e) {
            throw new RuntimeException(e);
        }
    }

}
