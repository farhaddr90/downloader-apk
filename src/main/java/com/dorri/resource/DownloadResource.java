package com.dorri.resource;

import com.akdeniz.googleplaycrawler.GooglePlayAPI;
import com.dorri.TransferManager;
import com.dorri.TransferWorker;
import com.dorri.core.Global;
import com.dorri.core.GoogleCommunication;
import com.dorri.racoon.gplay.SearchEngineResultPage;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@Path("/v1/api")
public class DownloadResource {

    @Inject
    TransferManager transferManager;

    @Inject
    TransferWorker transferWorker;

    @Inject
    GoogleCommunication google;

    @Inject
    Global global;

    @Path("/download")
    @GET
    public void getDownload() throws Exception {
        SearchEngineResultPage searchResult = google.search(global.get(GooglePlayAPI.class), "wolf");
        transferWorker.setDoc(searchResult.getContent().get(0));
        transferManager.schedule(transferWorker,0);
        System.out.println("salam");
    }
}
