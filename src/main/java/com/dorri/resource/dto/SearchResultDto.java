package com.dorri.resource.dto;

import com.akdeniz.googleplaycrawler.GooglePlay;
import com.dorri.racoon.gplay.DocUtil;

import java.util.List;

public class SearchResultDto {

    public String title;
    public String backendDocid;

//  public   ByteString auto1;
    public String auto2;
//  public   ByteString installNotesBytes;

    public List<String> permissions;

    public List<String> screenShots;

    public String devWeb;
    public String devEmail;
    public String play;
    public String video;
    public String desc;

    public SearchResultDto(GooglePlay.DocV2 doc) {
        title = doc.getTitle();
        backendDocid = doc.getBackendDocid();

//        auto1 = doc.getDetails().getAppDetails().getAutoAcquireFreeAppIfHigherVersionAvailableTagBytes();
        auto2 = doc.getDetails().getAppDetails().getAutoAcquireFreeAppIfHigherVersionAvailableTag();
//        installNotesBytes = doc.getDetails().getAppDetails().getInstallNotesBytes();

        permissions = doc.getDetails().getAppDetails()
                .getPermissionList();

        screenShots = DocUtil.getScreenShots(doc);

        devWeb = doc.getDetails().getAppDetails().getDeveloperWebsite();
        devEmail = doc.getDetails().getAppDetails().getDeveloperEmail();
        play = doc.getShareUrl();
        video = DocUtil.getVideoUrl(doc);
        StringBuilder sb = new StringBuilder(doc.getDescriptionHtml());
        sb.append("<p><hr><dl><dt>");
        sb.append(doc.getDetails().getAppDetails().getVersionString());
        sb.append("</dt><dd>");
        sb.append(doc.getDetails().getAppDetails().getRecentChangesHtml());
        sb.append("</dd><hr><ul>");
        if (video != null) {
            sb.append("<li> <a href=\"" + video + "\">"
                    + ".links.video" + "</a>");
        }
        sb.append("<li> <a href=\"" + play + "\">"
                + ".links.play" + "</a>");
        if (devWeb != null) {
            sb.append("<li> <a href=\"" + devWeb + "\">" +
                    ".links.dev.website" + "</a>");
        }
        if (devEmail != null) {
            sb.append("<li><a href=\"mailto:" + devEmail + "\">"
                    + ".links.dev.email" + "</a>");
        }
        sb.append("<li> <a href=\"");
        sb.append("\">");
        sb.append("</a>");

        desc = sb.toString();
    }

}
