package com.dorri.resource.dto;

import com.dorri.core.Global;
import com.dorri.racoon.Layout;
import com.reandroid.apkeditor.merge.Merger;
import com.reandroid.commons.command.ARGException;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.io.IOException;

@Path("/v1/merge")
public class MergerResource {

//    @Inject
//    Global global;

    @Path("do")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String merge(String packageName){
        try {
            StringBuilder str = new StringBuilder(Layout.DEFAULT.appsDir.getAbsolutePath());
            str.append("\\").append(packageName);
            Merger.execute(new String[]{"-i", str.toString()});
            return "successfully done!";
        } catch (ARGException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
