package com.dorri.resource;

import com.akdeniz.googleplaycrawler.GooglePlayAPI;
import com.dorri.racoon.PlayProfile;
import com.dorri.core.Global;
import com.dorri.core.InitLogin;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;


@Path("/v1/api")
public class LoginResource {

    @Inject
    Global global;
    @Inject
    InitLogin initLogin;

    @Path("/getToken")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getToken(){
        GooglePlayAPI api = global.get(GooglePlayAPI.class);
        if (api != null)
            return api.getToken();
        else
            return "api not exist in globals";
    }

    @Path("/profile")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getProfile(){
        PlayProfile pp = global.get(PlayProfile.class);
        if (pp != null)
            return pp.toString();
        else
            return "Profile not exist in globals";
    }

    @Path("/remove")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public void remove(String name) throws ClassNotFoundException {
        global.remove(Class.forName(name));
    }

    @Path("/reLogin")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response reLogin(PlayProfile pp) throws Exception {
        GooglePlayAPI init = initLogin.init(pp.getUser(), pp.getPassword());
        return Response.ok(init.getToken()).build();
    }

}
