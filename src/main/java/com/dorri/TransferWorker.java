package com.dorri;

import com.akdeniz.googleplaycrawler.DownloadData;
import com.akdeniz.googleplaycrawler.GooglePlay;
import com.akdeniz.googleplaycrawler.GooglePlayAPI;

import com.dorri.core.Global;
import com.dorri.racoon.Layout;
import com.dorri.racoon.PlayProfile;
import com.dorri.racoon.node.*;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

@ApplicationScoped
public class TransferWorker {

    @Inject
    Global global;

//    public static final String ID = AppDownloadWorker.class.getSimpleName();

//    private Globals globals;
    private InputStream inputStream;
    private OutputStream outputStream;

    private long totalBytes;
    private long bytesReceived;

    private int versionCode, offerType;
    private String packageName;
    private boolean paid;

    private AndroidApp download;
    private DownloadData data;
//    private TransferPeerBuilder control;

    private Layout layout;
    private PlayProfile profile;

    private File apkFile;
    private File iconFile;
    private File mainFile;
    private File patchFile;

    private int fileCount;
    private int splitCount;
    private String appName;

    //1-
    public void setDoc(GooglePlay.DocV2 doc){
        //        this.globals = globals;
        this.totalBytes = doc.getDetails().getAppDetails().getInstallationSize();
        layout = Layout.DEFAULT;
        versionCode = doc.getDetails().getAppDetails().getVersionCode();
        offerType = doc.getOffer(0).getOfferType();
        packageName = doc.getBackendDocid();
//        control = new TransferPeerBuilder(doc.getTitle());
//        control.withChannel(Messages.getString(ID + ".channel"));
//        globals.get(ImageLoaderService.class).request(control,
//                DocUtil.getAppIconUrl(doc));
//        profile = globals.get(DatabaseManager.class).get(PlayProfileDao.class)
//                .get();
        profile = global.get(PlayProfile.class);
        paid = doc.getOffer(0).getCheckoutFlowRequired();
        appName = doc.getTitle();
    }

    //2-
    public void onPrepare() throws Exception {
//        GooglePlayAPI api = global.get(PlayManager.class).createConnection();
        GooglePlayAPI api = global.get(GooglePlayAPI.class);
        if (paid) {
            // For apps that must be purchased before download
            data = api.delivery(packageName, versionCode, offerType);
        }
        else {
            // for apps that can be downloaded free of charge.
            data = api.purchaseAndDeliver(packageName, versionCode, offerType);
        }
//        data.setCompress(globals.get(Traits.class).isAvailable("4.0.x"));

        this.totalBytes = data.getTotalSize();
        apkFile = new AppInstallerNode(layout, packageName, versionCode).resolve();
        apkFile.getParentFile().mkdirs();
        iconFile = new AppIconNode(layout, packageName, versionCode).resolve();
        mainFile = new AppExpansionMainNode(layout, packageName,
                data.getMainFileVersion()).resolve();
        patchFile = new AppExpansionPatchNode(layout, packageName,
                data.getPatchFileVersion()).resolve();
        splitCount = data.getSplitCount();

    }

    // 3-
    public InputStream onNextSource() throws Exception {
        closeStreams();
        if (splitCount > 0) {
            splitCount--;
            outputStream = new FileOutputStream(new File(apkFile.getParentFile(),
                    data.getSplitId(splitCount) + "-" + versionCode + ".apk"));
            return data.openSplitDelivery(splitCount);
        }
        switch (fileCount) {
            case 0: {
                outputStream = new FileOutputStream(apkFile);
                inputStream = data.openApp();
                break;
            }
            case 1: {
                if (data.hasMainExpansion()) {
                    if (mainFile.exists()) {
                        // Looks like we are updating the app and the expansion of the
                        // previous
                        // version is still valid -> skip the download and make sure the
                        // file
                        // doesn't get deleted if the user cancels.
                        mainFile = null;
                        inputStream = new DevZeroInputStream(data.getMainSize());
                        outputStream = new DevNullOutputStream();
                    }
                    else {
                        inputStream = data.openMainExpansion();
                        outputStream = new FileOutputStream(mainFile);
                    }
                }
                break;
            }
            case 2: {
                if (data.hasPatchExpansion()) {
                    if (patchFile.exists()) {
                        // Looks like we are updating the app and the expansion of the
                        // previous
                        // version is still valid -> skip the download and make sure the
                        // file
                        // doesn't get deleted if the user cancels.
                        patchFile = null;
                        inputStream = new DevZeroInputStream(data.getPatchSize());
                        outputStream = new DevNullOutputStream();
                    }
                    else {
                        inputStream = data.openPatchExpansion();
                        outputStream = new FileOutputStream(patchFile);
                    }
                }
                break;
            }
        }
        fileCount++;
        return inputStream;
    }

    private void closeStreams() {
        try {
            inputStream.close();
            inputStream = null;
            outputStream.close();
            outputStream = null;
        }
        catch (Exception e) {
        }
    }

    public float onChunk(int size) {
        bytesReceived += size;
        return (float) bytesReceived / (float) totalBytes;
    }

    public OutputStream onNextDestination() throws Exception {
        return outputStream;
    }

    public void onIncomplete(Exception e) {
        if (apkFile != null) {
            apkFile.delete();
        }
        if (iconFile != null) {
            iconFile.delete();
        }
        if (mainFile != null) {
            mainFile.delete();
        }
        if (patchFile != null) {
            patchFile.delete();
        }
    }

//    public void onComplete() throws Exception {
//        AppIconNode ain = new AppIconNode(layout, packageName, versionCode);
//        iconFile = ain.resolve();
//        try {
//            ain.extractFrom(apkFile);
//        }
//        catch (IOException e) {
            // This is (probably) ok. Not all APKs have icons. Lets try to fall back
            // on what we have.
//            try {
//                Image img = control.iconImage;
//                BufferedImage bimage = new BufferedImage(img.getWidth(null),
//                        img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
//                Graphics2D bGr = bimage.createGraphics();
//                bGr.drawImage(img, 0, 0, null);
//                bGr.dispose();
//                ImageIO.write(bimage, "png", new FileOutputStream(ain.resolve()));
//            }
//            catch (Exception e2) {
//                // Nope, can't be helped.
//            }
//        }
//        download = AndroidAppDao.analyze(apkFile);
//        if (download.getName().startsWith("@string")) {
//            // Great, split APK ...
//            download.setName(appName);
//        }
//        if (data.hasMainExpansion()) {
//            download.setMainVersion(data.getMainFileVersion());
//        }
//        if (data.hasPatchExpansion()) {
//            download.setPatchVersion(data.getPatchFileVersion());
//        }
//        DatabaseManager dbm = globals.get(DatabaseManager.class);
//        dbm.get(AndroidAppDao.class).saveOrUpdate(download);
//        dbm.get(PlayAppOwnerDao.class).own(download, profile);
//    }

}
